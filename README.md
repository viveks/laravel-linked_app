## About Project docker-compose-laravel-linkedin

## Usage

-For the LinkedIn application setup steps, please refere to [https://docs.wpwebelite.com/social-network-integration/linkedin/](https://docs.wpwebelite.com/social-network-integration/linkedin/).

- In your LinkedIn app under OAuth 2.0 settings Authorized redirect URLs for your app should be <b>http://localhost:8080/auth/linkedin/callback</b>

- Copy .env.example to .env then update the your Linked App Authentication keys .env file

  - LINKEDIN_CLIENT_ID='your_linkedin_cliet_id'
  - LINKEDIN_SECRET_ID='your_linkedin_secret_id'

To get started, make sure you have [Docker installed](https://docs.docker.com/desktop/) on your system, and then clone this repository.

Next, navigate in your terminal to the directory you cloned this and by running `docker-compose up -d --build site`. containers for the web server.

Use the following command examples from your project root

- `docker-compose run --rm composer install`
- `docker-compose run --rm artisan key:generate`
- `docker-compose run --rm artisan migrate`

Next run the `docker-compose up -d` and then visit [http://localhost:8080/](http://localhost:8080/).
